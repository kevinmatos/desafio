@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Cadastro de produtos
                </div>
            </div>
        </div>
    </div>
</div>
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastrar Produtos</title>
</head>
<body>
    <form action="{{route('registrar_produto')}}" method="POST">
        @csrf
        <label for="">Nome</label><br />
        <input type="text"name="nome"><br />
        <label for="">Quantidade</label><br />
        <input type="text"name="quantidade"><br />
        <label for="">Preco</label><br />
        <input type="text"name="preco"><br />
        <button>Salvar</button>>
    </form>
    <form action="/produtos/adiciona" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="img_produtos" id="input_img_produtos">
            <label class="custom-file-label" for="input_img_produtos">Escolha o arquivo</label>
        </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Enviar</button>
    </form>
</body>
</html>
@endsection
