<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProdutosController extends Controller
{

    public function adicionaProdutos(ItensFormRequest $request)
    {
        // Handle File Upload
        if($request->hasFile('fotos')){
            // Get filename with the extension
            $filenameWithExt = $request->file('fotos')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('fotos')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('fotos')->storeAs('public/fotos', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.png';
        }
        //save in database
        $itens = produto::create([
            'preco' => $request->preco,
            'nome' => mb_strtolower($request->nome),
            'quantidade' => $request->quantidade,
            'categoria' => mb_strtolower($request->categoria),
            'img_itens' => $fileNameToStore
        ]);
        $request->session()->flash(
            'mensagem',
            "Produto {$produto->id} criad@ com sucesso {$produto->nome}"
        );
        return  redirect()->route('listar_produtos');
    }
}
